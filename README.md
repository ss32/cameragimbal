# cameraGimbal

WIP Control software for the 3D printed camera gimbal by [fhuable on Thingiverse](https://www.thingiverse.com/thing:3375167)

## Messages

The one-way message format is a simple serial command of the form 

```
tgt:X
```

where `X` is an integer value for the desired pan rotation in degrees.  All commands are currently relative to the current position of the gimbal's pan axis; no absolute positioning exists.  

