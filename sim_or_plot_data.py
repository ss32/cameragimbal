import numpy as np
import matplotlib.pylab as plt
import argparse
import cv2
import os
import tqdm


def make_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--file", type=str, default="log.csv")
    parser.add_argument("--dir", type=str, default="")
    parser.add_argument("--sim", action="store_true")
    return parser.parse_args()


def plot_data(
    t: np.array,
    x: np.array,
    y: np.array,
    pan_cmd: np.array,
    tilt_cmd: np.array,
    gimbal_commands: np.array,
):
    # plt.title("Controller Sim")
    plt.subplot(2, 2, 1)
    plt.plot(t, x, label="x")
    plt.plot(t, pan_cmd, label="ctrl_pan_cmd")
    plt.ylabel("Pixels")
    plt.legend(loc="upper left")
    plt.subplot(2, 2, 3)
    plt.plot(t, gimbal_commands[:, 0], label="gimbal_pan_cmd")
    plt.ylabel("Degrees")
    plt.legend(loc="lower left")
    plt.subplot(2, 2, 2)
    plt.plot(t, y, label="y")
    plt.plot(t, tilt_cmd, label="ctrl_tilt_cmd")
    plt.legend(loc="upper left")
    plt.subplot(2, 2, 4)
    plt.plot(t, gimbal_commands[:, 1], label="gimbal_tilt_cmd")
    plt.legend(loc="lower left")
    plt.show()


def simulate_pd(
    t: np.array, kp: float, kd: float, x: np.array, y: np.array, img_shape: tuple
):
    pan_cmd = np.zeros_like(x)
    tilt_cmd = np.zeros_like(y)
    for i in range(1, len(t)):
        dt = t[i] - t[i - 1]
        dx = img_shape[1] // 2 - x[i]
        dy = img_shape[0] // 2 - y[i]
        if abs(dx) < 45:
            dx = 0
        if abs(dy) < 45:
            dy = 0
        pan_cmd[i] = kp * dx + kd * (x[i] - x[i - 1]) / dt
        tilt_cmd[i] = kp * dy + kd * (y[i] - y[i - 1]) / dt
    return pan_cmd, tilt_cmd


def track_marker(img: np.array):
    # Isolate red color
    mask = cv2.inRange(img, (0, 0, 230), (50, 50, 255))
    # Convert the image to grayscale
    # Find contours
    contours, _ = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    if len(contours) == 0:
        return None
    # Find the largest contour
    largest_contour = max(contours, key=cv2.contourArea)
    # Get the bounding rectangle
    x, y, w, h = cv2.boundingRect(largest_contour)
    # Get the center of the rectangle
    center = (x + w // 2, y + h // 2)
    return center


def data_gen(dir: str):
    files = os.listdir(dir)
    files.sort()
    centers = []
    print("Generating data from directory...")
    for _, f in tqdm.tqdm(enumerate(files), total=len(files)):
        fullpath = os.path.join(dir, f)
        img = cv2.imread(fullpath)
        center = track_marker(img)
        if center is None:
            continue
        centers.append(center)
    return (np.array(centers), img.shape)


def make_log(
    data: np.array,
    filename: str,
    img_shape: tuple,
    dt: float,
    kp: float = 0.1,
    kd: float = 0.1,
):
    t = np.arange(0, len(data) * dt, dt)
    x = np.array([d[0] for d in data])
    y = np.array([d[1] for d in data])
    pan_cmd, tilt_cmd = simulate_pd(t, kp, kd, x, y, img_shape)
    with open(filename, "w") as f:
        f.write("dt,x,y,pan_cmd,tilt_cmd,img_w,img_h\n")
        for i in range(len(t)):
            f.write(
                f"{t[i]},{x[i]},{y[i]},{pan_cmd[i]},{tilt_cmd[i]}, {img_shape[1]}, {img_shape[0]}\n"
            )


# Linear mapping between image pixels and gimbal degrees
def convert_to_gimbal_commands(pan: np.array, tilt: np.array, img_shape: tuple):
    commands = np.zeros((len(pan), 2))
    for i in range(len(pan)):
        commands[i, 0] = pan[i] / img_shape[1] * 90
        commands[i, 1] = tilt[i] / img_shape[0] * 90
    return commands


def main():
    args = make_args()
    if args.dir != "":
        data, img_shape = data_gen(args.dir)
        make_log(data, args.file, img_shape, 0.3, 0.15, 0.1)
    t, x, y, pan_cmd, tilt_cmd, img_w, img_h = np.loadtxt(
        args.file, delimiter=",", unpack=True, skiprows=1
    )
    if args.sim:
        pan_cmd, tilt_cmd = simulate_pd(t, 0.1, 0.1, x, y, [img_w[0], img_h[0]])
    gimbal_commands = convert_to_gimbal_commands(
        pan_cmd, tilt_cmd, [img_w[0], img_h[0]]
    )
    plot_data(t, x, y, pan_cmd, tilt_cmd, gimbal_commands)


if __name__ == "__main__":
    main()
