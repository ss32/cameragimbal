import cv2

cap = cv2.VideoCapture('/dev/video0')
# cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
# cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1024)
# cap.set(cv2.CAP_PROP_EXPOSURE, 10) 
ret, frame = cap.read()
while ret:

    cv2.imshow('frame', frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    ret, frame = cap.read()