import cv2
import serial
import argparse
import numpy as np
import time

PAN_GEAR_RATIO = 8
TILT_GEAR_RATIO = 10
PAN_LIMITS = (-90, 90)
TILT_LIMITS = (-60, 60)
PAN_MM_PER_DEG = 77.5 / 90
TILT_MM_PER_DEG = 79.75 / 90
IMG_WIDTH = 1280
IMG_HEIGHT = 1024
marlin_commands = {
    "pan": "G0 X{:.2f}\n",
    "tilt": "G0 Y{:.2f}\n",
    "pan_tilt": "G0 X{:.2f} Y{:.2f}\n",
    "reltive_motion": "G91\n",
    "set_zero": "G92 X0 Y0\n",
    "disable_steppers": "M84\n",
    "display message": "M117 {}\n",
}


def make_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--dev", type=str, default="/dev/ttyACM0")
    parser.add_argument("--baudrate", type=int, default=115200)
    parser.add_argument("--camera", type=int, default=0)
    return parser.parse_args()


def track_marker(img: np.array):
    # Isolate red color
    # mask = cv2.inRange(img, (0, 0, 200), (50, 50, 255))
    # Convert the image to grayscale
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # # Threshold the image
    _, thresh = cv2.threshold(gray, 220, 255, cv2.THRESH_BINARY)
    # Find contours
    contours, _ = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    if len(contours) == 0:
        return None
    # Find the largest contour
    largest_contour = max(contours, key=cv2.contourArea)
    # Get the bounding rectangle
    x, y, w, h = cv2.boundingRect(largest_contour)
    # Get the center of the rectangle
    center = (x + w // 2, y + h // 2)
    return center


def put_marker_on_image(img: np.array, center: tuple):
    # Draw a circle around the center
    cv2.circle(img, center, 10, (0, 255, 0), -1)
    # Draw a 95x95 box in the center of the image
    cv2.rectangle(
        img,
        (img.shape[1] // 2 - 47, img.shape[0] // 2 - 47),
        (img.shape[1] // 2 + 47, img.shape[0] // 2 + 47),
        (0, 255, 0),
        2,
    )
    return img


def get_serial_connection(dev, baudrate=115200):
    try:
        ser = serial.Serial(dev, baudrate)
        r = ser.readline().decode("utf-8")
        if r.strip() != "start":
            raise Exception(f"Gimbal is not ready. Got response {r}")
        time.sleep(5)
        for i in range(11):
            r = ser.readline().decode("utf-8")
            print(r.strip())
        return ser
    except serial.SerialException as e:
        print(f"Error opening serial port: {e}")


# def convert_to_gimbal_coordinates(control: tuple, img_shape: tuple):
#     # Get the image center
#     img_center = (img_shape[0] // 2, img_shape[1] // 2)
#     # Get the difference between the image center and the marker center
#     dx = img_center[0]- center[0]
#     dy = img_center[1] - center[1]
#     # Convert the difference to gimbal coordinates
#     gimbal_dx = dx / img_shape[1] * 90
#     gimbal_dy = dy / img_shape[0] * 90
#     return gimbal_dx, gimbal_dy

def scale_to_gimbal_coords(pan_cmd, tilt_cmd, img_shape):
    pan_cmd = pan_cmd / img_shape[1] * 25
    tilt_cmd = tilt_cmd / img_shape[0] * 25
    return pan_cmd, tilt_cmd


def disable_steppers(ser):
    send_gimbal_command(ser, marlin_commands["disable_steppers"])


def go_to(ser, x, y):
    send_gimbal_command(ser, marlin_commands["pan_tilt"].format(x, y))


def send_gimbal_command(ser, cmd) -> bool:
    ser.write(cmd.encode("utf-8"))
    print(f"Sent {cmd.strip()}")
    response = ser.readline().decode("utf-8")

    if response.strip() != "ok" and response.strip() != "MMU not responding - DISABLED":
        print(f"Error sending {cmd} : {response}")
        return False
    return True


def get_positions(ser):
    ser.write(b"M114\n")
    response = ser.readline().decode("utf-8")
    # Parse line like X:0.00 Y:0.00 Z:0.15 E:0.00 Count X: 0.00 Y:0.00 Z:0.15 E:0.00 to print X and Y positions
    # Controller reponds with positions and 'ok'
    if response.strip() == "ok":
        response = ser.readline().decode("utf-8")
    if "MMU" in response.strip():
        response = ser.readline().decode("utf-8")
    x, y = response.split(" ")[0], response.split(" ")[1]
    x = x.split(":")[1]
    y = y.split(":")[1]
    return x, y


def print_lcd(ser, message):
    # LCD can fit 20 characters
    send_gimbal_command(ser, marlin_commands["display message"].format(message[0:20]))
    


def home(ser):
    x, y = get_positions(ser)
    dx = 100 - float(x)
    dy = 100 - float(y)
    send_gimbal_command(ser, marlin_commands["pan_tilt"].format(dx, dy))
    time.sleep(1)


def first_boot(ser):
    r = True
    #print_lcd(ser, "Boot sequence...")

    while r:
        r = send_gimbal_command(ser, marlin_commands["reltive_motion"])
        time.sleep(0.25)
        r = send_gimbal_command(ser, "G0 F4000 X0\n")
        time.sleep(0.25)
        r = send_gimbal_command(ser, "G0 F5000 Y0\n")
        time.sleep(0.25)
        # Set the axes to -100 which gives it +/- 100 "mm" of travel
        r = send_gimbal_command(ser, "G92 X100 Y100\n")
        disable_steppers(ser)
        time.sleep(0.25)
        x, y = get_positions(ser)
        print("Current positions: ", x, y)
        time.sleep(1.5)
        r = send_gimbal_command(
            ser,
            marlin_commands["pan_tilt"].format(
                PAN_MM_PER_DEG * 90, TILT_MM_PER_DEG * 90
            ),
        )
        time.sleep(1)
        x, y = get_positions(ser)
        print("Current positions: ", x, y)
        home(ser)
        time.sleep(1)
        r = send_gimbal_command(
            ser,
            marlin_commands["pan_tilt"].format(
                PAN_MM_PER_DEG * -90, TILT_MM_PER_DEG * -90
            ),
        )
        time.sleep(1)
        x, y = get_positions(ser)
        print("Current positions: ", x, y)
        home(ser)
        x, y = get_positions(ser)
        print("Current positions: ", x, y)
        disable_steppers(ser)
        #print_lcd(ser, "Boot complete")
        return

def pd_controller(dt, kp, kd, x, y, prev_x, prev_y, img_shape):
    dx_target = img_shape[1] // 2 - x
    dy_target = img_shape[0] // 2 - y
    dx = x - prev_x
    dy = y - prev_y

    pan = kp * dx_target + kd * dx / dt
    tilt = kp * dy_target + kd * dy / dt
    if abs(dx_target) < 45:
            pan = 0
    if abs(dy_target) < 45:
        tilt = 0
    return pan, tilt

def main():
    args = make_args()
    gimbal = get_serial_connection(args.dev, args.baudrate)
    if gimbal is None:
        print("Could not connect to gimbal")
        return
    print("Connected to gimbal")
    first_boot(gimbal)
    #print_lcd(gimbal, "Waiting on camera")
    cap = cv2.VideoCapture(args.camera)
    if cap.isOpened() is False:
        print("Error opening camera")
        return
    #cap = cv2.VideoCapture('target.mp4')
    # cap.set(cv2.CAP_PROP_FRAME_WIDTH, IMG_WIDTH)
    # cap.set(cv2.CAP_PROP_FRAME_HEIGHT, IMG_HEIGHT)
    logfile = open("log.csv", "w")
    logfile.write("time,pan_cmd,tilt_cmd,target_x,target_y\n")
    kp = 0.15
    kd = 0.01
    prev_x = 0
    prev_y = 0
    time_of_last_target = time.time()
    # vid_dt = 1.0 / cap.get(cv2.CAP_PROP_FPS)
    while True:
        ret, frame = cap.read()
        if not ret:
            break
        ts = time.time()
        # Resize the frame
        frame = cv2.resize(frame, (IMG_WIDTH, IMG_HEIGHT))
        center = track_marker(frame)
        dt = time.time() - ts
        if center is None:
            if time.time() - time_of_last_target > 3:
                #print_lcd(gimbal, "No target found")
                home(gimbal)
            continue
        if center is not None:
            frame = put_marker_on_image(frame, center)
            cv2.imshow("frame", frame)
            if cv2.waitKey(1) & 0xFF == ord("q"):
                break
            pan_cmd, tilt_cmd = pd_controller(dt, kp, kd, center[0], center[1], prev_x, prev_y, frame.shape)
            # Get the raw gimbal coords and check if they are within the limits
            # gimbal_frame_coords = convert_to_gimbal_coordinates(
            #     (pan_cmd, tilt_cmd), frame.shape
            # )
            scaled_pan, scaled_tilt = scale_to_gimbal_coords(pan_cmd, tilt_cmd, frame.shape)
            logfile.write(f"{ts},{center[0]},{center[1]},{scaled_pan},{scaled_tilt},{frame.shape[0]},{frame.shape[1]}\n")
            prev_x = center[0]
            prev_y = center[1]

            # Send the gimbal coords to the gimbal
            go_to(gimbal, scaled_pan, scaled_tilt)
            print(f'X{center[0]} Y{center[1]} G0 X{scaled_pan:.2f} Y{scaled_tilt:.2f}\n')
            time_of_last_target = time.time()
    cap.release()
    cv2.destroyAllWindows()
    logfile.close()
    home(gimbal)
    disable_steppers(gimbal)
    gimbal.close()


if __name__ == "__main__":
    main()
