/**
 * Author Teemu Mäntykallio
 * Initializes the library and runs the stepper
 * motor in alternating directions.
 */

#include <TMCStepper.h>
// Mega
// #define DIR_PIN   8 //direction
// #define STEP_PIN  9 //step
// #define SW_MISO 50 //SDO/MISO
// #define CS_PAN   10 //CS chip select
// #define SW_SCK  52 //CLK/SCK
// #define SW_MOSI 51 //SDI/MOSI
// #define EN_PIN    7 //enable

// Nano
// #define DIR_PIN   5 //direction
// #define STEP_PIN  6 //step
// #define SW_MISO 12 //SDO/MISO
// #define CS_PAN   10 //CS chip select
// #define SW_SCK  13 //CLK/SCK
// #define SW_MOSI 11 //SDI/MOSI
// #define EN_PIN    7 //enable

// ESP32
// Common SPI bus pins for both devices
#define SW_SCK 18  // CLK/SCK
#define SW_MOSI 23 // SDI/MOSI
#define SW_MISO 19 // SDO/MISO

// Pan
#define DIR_PAN 17  // direction
#define STEP_PAN 22 // step
#define CS_PAN 21   // CS chip select
#define EN_PIN 4    // enable

// Tilt
#define EN_TILT 26
#define DIR_TILT 27
#define STEP_TILT 32
#define CS_TILT 33

#define R_SENSE 0.11f // Match to your driver
                      // SilentStepStick series use 0.11
                      // UltiMachine Einsy and Archim2 boards use 0.2
                      // Panucatt BSD2660 uses 0.1
                      // Watterott TMC5160 uses 0.075

// Select your stepper driver type
TMC2130Stepper driver(CS_PAN, R_SENSE);   // Hardware SPI
TMC2130Stepper driver2(CS_TILT, R_SENSE); // Hardware SPI

#define STEPS_PER_REVOLUTION 200
#define MICROSTEPS 128
#define GEAR_RATIO 8

int wait_time = 1;
float deg = 0;
bool flipped = true;
int steps_per_rev = STEPS_PER_REVOLUTION * MICROSTEPS;
float deg_per_step = 360.0 / steps_per_rev;
int32_t current_pan_position = 0;
int32_t current_tilt_position = 0;
bool got_target_position = false;
const String target_position_key = "tgt:";
float values[2];
bool pan_done = false;
bool tilt_done = false;
int32_t pan_target = 0;
int32_t tilt_target = 0;

void extractValues(String str, String key)
{
  int keyIndex = str.indexOf(key);
  if (keyIndex == -1)
  {
    values[0] = -1;
    values[1] = -1;
  }
  int startIndex = keyIndex + key.length();
  int endIndex = str.indexOf(",", startIndex);
  String value = str.substring(startIndex, endIndex);
  values[0] = value.toFloat();
  int nextIndex = endIndex + 1;
  value = str.substring(nextIndex, str.indexOf(";", nextIndex));
  values[1] = value.toFloat();
}

bool findString(String str, String key)
{
  return str.indexOf(key) != -1;
}

void setup()
{
  pinMode(EN_PIN, OUTPUT);
  pinMode(EN_TILT, OUTPUT);
  pinMode(STEP_PAN, OUTPUT);
  pinMode(DIR_PAN, OUTPUT);
  pinMode(DIR_TILT, OUTPUT);
  pinMode(STEP_TILT, OUTPUT);
  digitalWrite(EN_PIN, LOW);  // Enable driver in hardware
  digitalWrite(EN_TILT, LOW); // Enable driver in hardware
  digitalWrite(DIR_PAN, HIGH);
  digitalWrite(DIR_TILT, HIGH);
  Serial.begin(115200);
  Serial.setTimeout(1);

  SPI.begin(); // SPI drivers

  driver.begin();                //  SPI: Init CS pins and possible SW SPI pins
                                 // UART: Init SW UART (if selected) with default 115200 baudrate
  driver.toff(5);                // Enables driver in software
  driver.rms_current(700);       // Set motor RMS current
  driver.microsteps(MICROSTEPS); // Set microsteps

  driver.en_pwm_mode(true);   // Toggle stealthChop on TMC2130/2160/5130/5160
  driver.pwm_autoscale(true); // Needed for stealthChop

  driver2.begin();
  driver2.toff(5);                // Enables driver in software
  driver2.rms_current(700);       // Set motor RMS current
  driver2.microsteps(MICROSTEPS); // Set microsteps
  driver2.en_pwm_mode(true);      // Toggle stealthChop on TMC2130/2160/5130/5160
  driver2.pwm_autoscale(true);    // Needed for stealthChop
}

void loop()
{
  if (Serial.available() > 0)
  {
    String data = Serial.readString();
    if (findString(data, target_position_key) && !got_target_position)
    {
      extractValues(data, target_position_key);
      if (values[0] != -1 && values[1] != -1)
      {

        got_target_position = true;
        if (values[0] < -1)
        {
          digitalWrite(DIR_PAN, LOW);
          values[0] *= -1;
        }
        else
        {
          digitalWrite(DIR_PAN, HIGH);
        }
        if (values[1] < -1)
        {
          digitalWrite(DIR_TILT, LOW);
          values[1] *= -1;
        }
        else
        {
          digitalWrite(DIR_TILT, HIGH);
        }
        pan_target = int16_t(values[0] / deg_per_step);
        tilt_target = int16_t(values[1] / deg_per_step);
        Serial.println("Got target positions: " + String(pan_target) + "," + String(tilt_target));
      }
    }
  }
  if (got_target_position)
  {

    if (current_pan_position >= pan_target)
    {
      pan_done = true;
    }
    else
    {
      digitalWrite(CS_PAN, LOW);
      digitalWrite(CS_TILT, HIGH);
      digitalWrite(STEP_PAN, HIGH);
      delayMicroseconds(wait_time);
      digitalWrite(STEP_PAN, LOW);
      delayMicroseconds(wait_time);
      current_pan_position++;
    }
    if (current_tilt_position >= tilt_target)
    {
      tilt_done = true;
    }
    else
    {
      digitalWrite(CS_PAN, HIGH);
      digitalWrite(CS_TILT, LOW);
      digitalWrite(STEP_TILT, HIGH);
      delayMicroseconds(wait_time);
      digitalWrite(STEP_TILT, LOW);
      delayMicroseconds(wait_time);
      current_tilt_position++;
    }

    if (pan_done && tilt_done)
    {
      pan_done = false;
      tilt_done = false;
      pan_target = 0;
      tilt_target = 0;
      got_target_position = false;
      Serial.println("Done");
      values[0] = -1;
      values[1] = -1;
    }
  }
}
